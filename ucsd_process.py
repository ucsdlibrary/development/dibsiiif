import os
import shutil
from pathlib import Path
from commonpy.network_utils import net
from decouple import config
import traceback
from PIL import Image, ImageSequence
import plac

# import json
# import subprocess
# import sys


def main(barcode: "the barcode of an item to be processed"):
    '''Process an item for UC San Diego Library DIBS.'''

    try:
        (
            PROCESSED_IIIF_DIR,
            PROCESSED_SCANS_DIR,
            STATUS_FILES_DIR,
            UNPROCESSED_SCANS_DIR,
            VIPS_CMD,
        ) = validate_settings()
    except Exception as e:
        # NOTE we cannot guarantee that `STATUS_FILES_DIR` is set
        # - it must exist if script is started from `initiated.sh`
        # TODO figure out how to not send a message every minute
        message = "❌ there was a problem with the settings for the `dibsiiif.py` script"
        print(message)
        raise

    # remove `STATUS_FILES_DIR/{barcode}-initiated` file
    # NOTE in order to allow the script to be run indpendently of a
    # wrapper, we should not insist upon the initiated file existing
    try:
        Path(STATUS_FILES_DIR).joinpath(f"{barcode}-initiated").unlink(missing_ok=True)
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

    # create `STATUS_FILES_DIR/{barcode}-processing` file
    try:
        Path(STATUS_FILES_DIR).joinpath(f"{barcode}-processing").touch(exist_ok=False)
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

    # validate the `UNPROCESSED_SCANS_DIR/{barcode}` directory
    try:
        barcode_dir = Path(UNPROCESSED_SCANS_DIR).joinpath(barcode).resolve(strict=True)
        if not len(os.listdir(barcode_dir)):
            raise ValueError(f"item directory is empty: {barcode_dir}")
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

    # Split single multipage TIF into many individual tifs
    single_tif = Image.open(f'{barcode_dir}/{barcode}.tif')
    for i, page in enumerate(ImageSequence.Iterator(single_tif)):
        page.save(f'{barcode_dir}/{barcode}_{i:04}.tif', compression=None)

    # Rename original tif so it's not processed in next section
    os.rename(f'{barcode_dir}/{barcode}.tif', f'{barcode_dir}/{barcode}.org')

    tiff_paths = []
    sequence = []
    for i in os.scandir(barcode_dir):
        if i.is_file() and i.name.endswith((".tif", ".tiff")):
            # for the case of `35047000000000_001.tif`
            if (
                len(i.name.split(".")[0].split("_")) == 2
                and i.name.split(".")[0].split("_")[0] == barcode
                and i.name.split(".")[0].split("_")[-1].isnumeric()
            ):
                tiff_paths.append(i.path)
                sequence.append(int(i.name.split(".")[0].split("_")[-1]))
            else:
                print(
                    f" ⚠️\t unexpected file name format encountered: {barcode}/{i.name}"
                )
    # verify that TIFFs exist in the `{barcode_dir}`
    try:
        if not len(tiff_paths):
            raise ValueError(f"item directory contains no TIFFs: {barcode_dir}")
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

    # raise exception if the sequence is missing any numbers
    try:
        if missing_numbers(sequence):
            raise ValueError(
                f"missing sequence numbers for {barcode}: {missing_numbers(sequence)}"
            )
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise
    
    # make IIIF directory if needed
    try:
        os.makedirs(f"{PROCESSED_IIIF_DIR}/{barcode}", exist_ok=True)
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)

    #  Process input Tifs here and put them in the {PROCESSED_IIIF_DIR}/{barcode}
        # loop through sorted list of TIFF paths
    tiff_paths.sort()
    for f in tiff_paths:
        f = Path(f)
        # create compressed pyramid TIFF
        if (
            # TODO use subprocess.run()
            os.system(
                f"{VIPS_CMD} tiffsave {f} {PROCESSED_IIIF_DIR}/{barcode}/{f.stem.split('_')[-1]}.tif --tile --pyramid --compression jpeg --tile-width 256 --tile-height 256"
            )
            != 0
        ):
            print("❌ an error occurred running the vips command")
            raise RuntimeError(
                f"{VIPS_CMD} tiffsave {f} {PROCESSED_IIIF_DIR}/{barcode}/{f.stem.split('_')[-1]}.tif --tile --pyramid --compression jpeg --tile-width 256 --tile-height 256"
            )
        # create canvas metadata
        # HACK the binaries for `vips` and `vipsheader` should be in the same place
        # width = os.popen(f"{VIPS_CMD}header -f width {f}").read().strip()
        # height = os.popen(f"{VIPS_CMD}header -f height {f}").read().strip()

    # Now that the final tifs have been created, remove the intermediate tifs before we move the barcode_dir
    for file in barcode_dir.glob('*.tif'):
        try:
            file.unlink()
        except:
            print(f'Error deleting {file}')

    # move `barcode_dir` into `PROCESSED_SCANS_DIR`
    # NOTE shutil.move() in Python < 3.9 needs strings as arguments
    try:
        shutil.move(str(barcode_dir), str(PROCESSED_SCANS_DIR))
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

    # remove `STATUS_FILES_DIR/{barcode}-processing` file
    try:
        Path(STATUS_FILES_DIR).joinpath(f"{barcode}-processing").unlink()
    except Exception as e:
        with open(Path(STATUS_FILES_DIR).joinpath(f"{barcode}-problem"), "w") as f:
            traceback.print_exc(file=f)
        raise

def missing_numbers(sequence):
    """return a list of missing sequence numbers"""
    sequence.sort()
    return [x for x in range(sequence[0], sequence[-1] + 1) if x not in sequence]

def validate_settings():
    
    PROCESSED_IIIF_DIR = directory_setup(
        os.path.expanduser(config("PROCESSED_IIIF_DIR"))
    ).resolve(strict=True)
    
    PROCESSED_SCANS_DIR = directory_setup(
        os.path.expanduser(config("PROCESSED_SCANS_DIR"))
    ).resolve(strict=True)
    
    STATUS_FILES_DIR = Path(os.path.expanduser(config("STATUS_FILES_DIR"))).resolve(
        strict=True
    )  # NOTE do not create missing `STATUS_FILES_DIR`
    
    UNPROCESSED_SCANS_DIR = directory_setup(
        os.path.expanduser(config("UNPROCESSED_SCANS_DIR"))
    ).resolve(strict=True)
    
    VIPS_CMD = Path(os.path.expanduser(config("VIPS_CMD"))).resolve(strict=True)
    return (
        PROCESSED_IIIF_DIR,
        PROCESSED_SCANS_DIR,
        STATUS_FILES_DIR,
        UNPROCESSED_SCANS_DIR,
        VIPS_CMD,
    )

def directory_setup(directory):
    if not Path(directory).exists():
        Path(directory).mkdir()
    elif Path(directory).is_file():
        print(f" ❌\t A non-directory file exists at: {directory}")
        raise FileExistsError
    return Path(directory)

if __name__ == "__main__":
    plac.call(main)
